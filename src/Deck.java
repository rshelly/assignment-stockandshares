import java.util.Random;
import java.util.ArrayList;

import javax.swing.ImageIcon;

/**
 * @author Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version 1.0.0
 */

public class Deck {

  private ArrayList<Card> deck;

  public Card steelsDown4;
  public Card steelsDown3;
  public Card steelsDown2;
  public Card steelsUp4;
  public Card steelsUp3;
  public Card steelsUp2;

  public Card storesDown4;
  public Card storesDown3;
  public Card storesDown2;
  public Card storesUp4;
  public Card storesUp3;
  public Card storesUp2;

  public Card motorsDown4;
  public Card motorsDown3;
  public Card motorsDown2;
  public Card motorsUp4;
  public Card motorsUp3;
  public Card motorsUp2;

  public Card shippingDown4;
  public Card shippingDown3;
  public Card shippingDown2;
  public Card shippingUp4;
  public Card shippingUp3;
  public Card shippingUp2;

  public Card bear;
  public Card bull;

  /**
   * Construct a deck of cards Contains the 26 cards from the game
   */
  public Deck() {
    steelsDown4 = new Card("steels", -4, new ImageIcon(this.getClass()
        .getResource("/steelsdown4.jpg")).getImage());
    steelsDown3 = new Card("steels", -3, new ImageIcon(this.getClass()
        .getResource("/steelsdown3.jpg")).getImage());
    steelsDown2 = new Card("steels", -2, new ImageIcon(this.getClass()
        .getResource("/steelsdown2.jpg")).getImage());
    steelsUp4 = new Card("steels", +4, new ImageIcon(this.getClass()
        .getResource("/steelsup4.jpg")).getImage());
    steelsUp3 = new Card("steels", +3, new ImageIcon(this.getClass()
        .getResource("/steelsup3.jpg")).getImage());
    steelsUp2 = new Card("steels", +2, new ImageIcon(this.getClass()
        .getResource("/steelsup2.jpg")).getImage());

    storesDown4 = new Card("stores", -4, new ImageIcon(this.getClass()
        .getResource("/storesdown4.jpg")).getImage());
    storesDown3 = new Card("stores", -3, new ImageIcon(this.getClass()
        .getResource("/storesdown3.jpg")).getImage());
    storesDown2 = new Card("stores", -2, new ImageIcon(this.getClass()
        .getResource("/storesdown2.jpg")).getImage());
    storesUp4 = new Card("stores", +4, new ImageIcon(this.getClass()
        .getResource("/storesup4.jpg")).getImage());
    storesUp3 = new Card("stores", +3, new ImageIcon(this.getClass()
        .getResource("/storesup3.jpg")).getImage());
    storesUp2 = new Card("stores", +2, new ImageIcon(this.getClass()
        .getResource("/storesup2.jpg")).getImage());

    motorsDown4 = new Card("motors", -4, new ImageIcon(this.getClass()
        .getResource("/motorsdown4.jpg")).getImage());
    motorsDown3 = new Card("motors", -3, new ImageIcon(this.getClass()
        .getResource("/motorsdown3.jpg")).getImage());
    motorsDown2 = new Card("motors", -2, new ImageIcon(this.getClass()
        .getResource("/motorsdown2.jpg")).getImage());
    motorsUp4 = new Card("motors", +4, new ImageIcon(this.getClass()
        .getResource("/motorsup4.jpg")).getImage());
    motorsUp3 = new Card("motors", +3, new ImageIcon(this.getClass()
        .getResource("/motorsup3.jpg")).getImage());
    motorsUp2 = new Card("motors", +2, new ImageIcon(this.getClass()
        .getResource("/motorsup2.jpg")).getImage());

    shippingDown4 = new Card("shipping", -4, new ImageIcon(this.getClass()
        .getResource("/shippingdown4.jpg")).getImage());
    shippingDown3 = new Card("shipping", -3, new ImageIcon(this.getClass()
        .getResource("/shippingdown3.jpg")).getImage());
    shippingDown2 = new Card("shipping", -2, new ImageIcon(this.getClass()
        .getResource("/shippingdown2.jpg")).getImage());
    shippingUp4 = new Card("shipping", +4, new ImageIcon(this.getClass()
        .getResource("/shippingup4.jpg")).getImage());
    shippingUp3 = new Card("shipping", +3, new ImageIcon(this.getClass()
        .getResource("/shippingup3.jpg")).getImage());
    shippingUp2 = new Card("shipping", +2, new ImageIcon(this.getClass()
        .getResource("/shippingup2.jpg")).getImage());

    bear = new Card("all", -4, new ImageIcon(this.getClass().getResource(
        "/bear.jpg")).getImage());

    bull = new Card("all", +4, new ImageIcon(this.getClass().getResource(
        "/bull.jpg")).getImage());

    deck = new ArrayList<Card>();

    deck.add(steelsDown4);
    deck.add(steelsDown3);
    deck.add(steelsDown2);
    deck.add(steelsUp4);
    deck.add(steelsUp3);
    deck.add(steelsUp2);
    deck.add(storesDown4);
    deck.add(storesDown3);
    deck.add(storesDown2);
    deck.add(storesUp4);
    deck.add(storesUp3);
    deck.add(storesUp2);
    deck.add(motorsDown4);
    deck.add(motorsDown3);
    deck.add(motorsDown2);
    deck.add(motorsUp4);
    deck.add(motorsUp3);
    deck.add(motorsUp2);
    deck.add(shippingDown4);
    deck.add(shippingDown3);
    deck.add(shippingDown2);
    deck.add(shippingUp4);
    deck.add(shippingUp3);
    deck.add(shippingUp2);
    deck.add(bear);
    deck.add(bull);
  }

  /**
   * Getter for a full deck of cards
   * 
   * @return Deck containing all cards
   */
  public ArrayList<Card> getDeck() {
    return deck;
  }
}