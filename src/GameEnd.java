import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;
import java.awt.Color;

/**
 * @author  Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version    1.0.0
 */

public class GameEnd extends JDialog {

  private ArrayList<Player> players;
  private StockMarket stockMarket;
  private Player winner;

  private JButton btnExit;
  private JButton btnPlayAgain;

  private JLabel lblCongrats;
  private JLabel lblWinner;
  private JLabel lblDraw;
  private JLabel lblDraw2;
  private JLabel lblGameOver;

  // ScoreBoard Components
  private JLabel lblMoney;
  private JLabel lblAssets;
  private JLabel lblTotal;
  private JLabel lblPlayer1;
  private JLabel lblPlayer2;
  private JLabel lblPlayer3;
  private JLabel lblPlayer4;
  private JLabel lblPlayer5;
  private JLabel lblPlayer6;
  private JTextField txtPlayer1Money;
  private JTextField txtPlayer2Money;
  private JTextField txtPlayer3Money;
  private JTextField txtPlayer4Money;
  private JTextField txtPlayer5Money;
  private JTextField txtPlayer6Money;
  private JTextField txtPlayer1Assets;
  private JTextField txtPlayer2Assets;
  private JTextField txtPlayer3Assets;
  private JTextField txtPlayer4Assets;
  private JTextField txtPlayer5Assets;
  private JTextField txtPlayer6Assets;
  private JTextField txtPlayer1Total;
  private JTextField txtPlayer2Total;
  private JTextField txtPlayer3Total;
  private JTextField txtPlayer4Total;
  private JTextField txtPlayer5Total;
  private JTextField txtPlayer6Total;

  private JLabel lblMotors;
  private JLabel lblShipping;
  private JLabel lblSteels;
  private JLabel lblStores;
  private JLabel lblMotorsValue;
  private JLabel lblShippingValue;
  private JLabel lblSteelsValue;
  private JLabel lblStoresValue;

  /**
   * Launches the dialog box
   * Takes in an ArrayList of players and the stock market as parameters
   * Calls the constructor to build the dialog box
   * 
   * @param players The ArrayList of players playing the game
   * @param stockMarket The stock market with current share prices
   */
  public static void main(ArrayList<Player> players, StockMarket stockMarket) {
    try {
      GameEnd dialog = new GameEnd(players, stockMarket);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      //Sets the Dialog Box's modality to application
      //This means parent window will pause and will not accept user interaction
      //until Dialog Box has closed
      dialog.setModalityType(ModalityType.APPLICATION_MODAL);
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Builds the dialog box
   * Takes in an ArrayList of players and the stock market as parameters
   *  and sets them in the class fields so they can be used in all methods
   * 
   * @param players The ArrayList of players playing the game
   * @param stockMarket The stock market with current share prices
   */
  public GameEnd(ArrayList<Player> players, StockMarket stockMarket) {
    getContentPane().setBackground(Color.RED);
    setBounds(150, 100, 640, 480);
    getContentPane().setLayout(null);

    setPlayers(players);
    setStockMarket(stockMarket);
    calculatePlayerAssets();
    setWinner();

    btnExit = new JButton("Exit");
    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        System.exit(EXIT_ON_CLOSE);
      }
    });
    btnExit.setBounds(514, 400, 100, 30);
    getContentPane().add(btnExit);

    btnPlayAgain = new JButton("Play Again");
    btnPlayAgain.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        // Just closes the GameEnd dialog box
        // Restarting the game is then achieved back in the parent window
        dispose();
      }
    });
    btnPlayAgain.setBounds(404, 400, 100, 30);
    getContentPane().add(btnPlayAgain);

    lblGameOver = new JLabel("Game Over");
    lblGameOver.setHorizontalAlignment(SwingConstants.CENTER);
    lblGameOver.setFont(new Font("Tahoma", Font.PLAIN, 48));
    lblGameOver.setBounds(195, 11, 250, 50);
    getContentPane().add(lblGameOver);

    lblMoney = new JLabel("Money");
    lblMoney.setBounds(20, 295, 70, 20);
    getContentPane().add(lblMoney);

    lblAssets = new JLabel("Assets");
    lblAssets.setBounds(20, 320, 70, 20);
    getContentPane().add(lblAssets);

    lblTotal = new JLabel("Total");
    lblTotal.setBounds(20, 345, 70, 20);
    getContentPane().add(lblTotal);

    lblPlayer1 = new JLabel("player1");
    lblPlayer1.setBounds(90, 270, 70, 20);
    getContentPane().add(lblPlayer1);

    lblPlayer2 = new JLabel();
    lblPlayer2.setBounds(180, 270, 70, 20);
    getContentPane().add(lblPlayer2);

    lblPlayer3 = new JLabel();
    lblPlayer3.setBounds(270, 270, 70, 20);
    getContentPane().add(lblPlayer3);

    lblPlayer4 = new JLabel();
    lblPlayer4.setBounds(360, 270, 70, 20);
    getContentPane().add(lblPlayer4);
    lblPlayer4.setVisible(false);

    lblPlayer5 = new JLabel();
    lblPlayer5.setBounds(450, 270, 70, 20);
    getContentPane().add(lblPlayer5);
    lblPlayer5.setVisible(false);

    lblPlayer6 = new JLabel();
    lblPlayer6.setBounds(540, 270, 70, 20);
    getContentPane().add(lblPlayer6);
    lblPlayer6.setVisible(false);

    txtPlayer1Money = new JTextField();
    txtPlayer1Money.setBounds(90, 295, 70, 20);
    getContentPane().add(txtPlayer1Money);

    txtPlayer2Money = new JTextField();
    txtPlayer2Money.setBounds(180, 295, 70, 20);
    getContentPane().add(txtPlayer2Money);

    txtPlayer3Money = new JTextField();
    txtPlayer3Money.setBounds(270, 295, 70, 20);
    getContentPane().add(txtPlayer3Money);

    txtPlayer4Money = new JTextField();
    txtPlayer4Money.setBounds(360, 295, 70, 20);
    getContentPane().add(txtPlayer4Money);
    txtPlayer4Money.setVisible(false);

    txtPlayer5Money = new JTextField();
    txtPlayer5Money.setBounds(450, 295, 70, 20);
    getContentPane().add(txtPlayer5Money);
    txtPlayer5Money.setVisible(false);

    txtPlayer6Money = new JTextField();
    txtPlayer6Money.setBounds(540, 295, 70, 20);
    getContentPane().add(txtPlayer6Money);
    txtPlayer6Money.setVisible(false);

    txtPlayer1Assets = new JTextField();
    txtPlayer1Assets.setBounds(90, 320, 70, 20);
    getContentPane().add(txtPlayer1Assets);

    txtPlayer2Assets = new JTextField();
    txtPlayer2Assets.setBounds(180, 320, 70, 20);
    getContentPane().add(txtPlayer2Assets);

    txtPlayer3Assets = new JTextField();
    txtPlayer3Assets.setBounds(270, 320, 70, 20);
    getContentPane().add(txtPlayer3Assets);

    txtPlayer4Assets = new JTextField();
    txtPlayer4Assets.setBounds(360, 320, 70, 20);
    getContentPane().add(txtPlayer4Assets);
    txtPlayer4Assets.setVisible(false);

    txtPlayer5Assets = new JTextField();
    txtPlayer5Assets.setBounds(450, 320, 70, 20);
    getContentPane().add(txtPlayer5Assets);
    txtPlayer5Assets.setVisible(false);

    txtPlayer6Assets = new JTextField();
    txtPlayer6Assets.setBounds(540, 320, 70, 20);
    getContentPane().add(txtPlayer6Assets);
    txtPlayer6Assets.setVisible(false);

    txtPlayer1Total = new JTextField();
    txtPlayer1Total.setBounds(90, 345, 70, 20);
    getContentPane().add(txtPlayer1Total);

    txtPlayer2Total = new JTextField();
    txtPlayer2Total.setBounds(180, 345, 70, 20);
    getContentPane().add(txtPlayer2Total);

    txtPlayer3Total = new JTextField();
    txtPlayer3Total.setBounds(270, 345, 70, 20);
    getContentPane().add(txtPlayer3Total);

    txtPlayer4Total = new JTextField();
    txtPlayer4Total.setBounds(360, 345, 70, 20);
    getContentPane().add(txtPlayer4Total);
    txtPlayer4Total.setVisible(false);

    txtPlayer5Total = new JTextField();
    txtPlayer5Total.setBounds(450, 345, 70, 20);
    getContentPane().add(txtPlayer5Total);
    txtPlayer5Total.setVisible(false);

    txtPlayer6Total = new JTextField();
    txtPlayer6Total.setBounds(540, 345, 70, 20);
    getContentPane().add(txtPlayer6Total);
    txtPlayer6Total.setVisible(false);

    lblCongrats = new JLabel("Congratulations " + winner.getPlayerName()
        + "!!!");
    lblCongrats.setFont(new Font("Tahoma", Font.PLAIN, 32));
    lblCongrats.setHorizontalAlignment(SwingConstants.CENTER);
    lblCongrats.setBounds(20, 72, 594, 50);
    getContentPane().add(lblCongrats);

    lblWinner = new JLabel("You are the Winner");
    lblWinner.setBounds(20, 133, 594, 50);
    lblWinner.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblWinner);

    lblDraw = new JLabel("We have a Draw");
    lblDraw.setFont(new Font("Tahoma", Font.PLAIN, 32));
    lblDraw.setHorizontalAlignment(SwingConstants.CENTER);
    lblDraw.setBounds(20, 72, 594, 50);
    lblDraw.setVisible(false);
    getContentPane().add(lblDraw);

    lblDraw2 = new JLabel("Please see the scoreboard!");
    lblDraw2.setBounds(20, 133, 594, 50);
    lblDraw2.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblDraw2);
    lblDraw2.setVisible(false);

    lblMotors = new JLabel("Motors");
    lblMotors.setBounds(118, 194, 100, 15);
    lblMotors.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblMotors);

    lblShipping = new JLabel("Shipping");
    lblShipping.setBounds(218, 194, 100, 15);
    lblShipping.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblShipping);

    lblSteels = new JLabel("Steels");
    lblSteels.setBounds(418, 194, 100, 15);
    lblSteels.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblSteels);

    lblStores = new JLabel("Stores");
    lblStores.setBounds(318, 194, 100, 15);
    lblStores.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblStores);

    lblMotorsValue = new JLabel("$" + stockMarket.getStockPrice("motors"));
    lblMotorsValue.setBounds(118, 219, 100, 15);
    lblMotorsValue.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblMotorsValue);

    lblShippingValue = new JLabel("$" + stockMarket.getStockPrice("shipping"));
    lblShippingValue.setBounds(218, 219, 100, 15);
    lblShippingValue.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblShippingValue);

    lblSteelsValue = new JLabel("$" + stockMarket.getStockPrice("steels"));
    lblSteelsValue.setBounds(418, 219, 100, 15);
    lblSteelsValue.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblSteelsValue);

    lblStoresValue = new JLabel("$" + stockMarket.getStockPrice("stores"));
    lblStoresValue.setBounds(318, 219, 100, 15);
    lblStoresValue.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(lblStoresValue);

    setWindows();
    checkForTie();
  }

  /**
   * Sets the text of the necessary texts fields Sets the necessary contents for
   * the window as visible
   */
  public void setWindows() {

    lblPlayer1.setText(players.get(0).getPlayerName());
    lblPlayer2.setText(players.get(1).getPlayerName());
    lblPlayer3.setText(players.get(2).getPlayerName());

    txtPlayer1Money.setText("$" + players.get(0).getPlayerMoney());
    txtPlayer2Money.setText("$" + players.get(1).getPlayerMoney());
    txtPlayer3Money.setText("$" + players.get(2).getPlayerMoney());

    txtPlayer1Assets.setText("$" + players.get(0).getAssets());
    txtPlayer2Assets.setText("$" + players.get(1).getAssets());
    txtPlayer3Assets.setText("$" + players.get(2).getAssets());

    txtPlayer1Total.setText("$" + players.get(0).getTotal());
    txtPlayer2Total.setText("$" + players.get(1).getTotal());
    txtPlayer3Total.setText("$" + players.get(2).getTotal());

    if (players.size() >= 4) {

      lblPlayer4.setVisible(true);
      txtPlayer4Money.setVisible(true);
      txtPlayer4Assets.setVisible(true);
      txtPlayer4Total.setVisible(true);

      lblPlayer4.setText(players.get(3).getPlayerName());
      txtPlayer4Money.setText("$" + players.get(3).getPlayerMoney());
      txtPlayer4Assets.setText("$" + players.get(3).getAssets());
      txtPlayer4Total.setText("$" + players.get(3).getTotal());
    }
    if (players.size() >= 5) {

      lblPlayer5.setVisible(true);
      txtPlayer5Money.setVisible(true);
      txtPlayer5Assets.setVisible(true);
      txtPlayer5Total.setVisible(true);

      lblPlayer5.setText(players.get(4).getPlayerName());
      txtPlayer5Money.setText("$" + players.get(4).getPlayerMoney());
      txtPlayer5Assets.setText("$" + players.get(4).getAssets());
      txtPlayer5Total.setText("$" + players.get(4).getTotal());
    }
    if (players.size() == 6) {

      lblPlayer6.setVisible(true);
      txtPlayer6Money.setVisible(true);
      txtPlayer6Assets.setVisible(true);
      txtPlayer6Total.setVisible(true);

      lblPlayer6.setText(players.get(5).getPlayerName());
      txtPlayer6Money.setText("$" + players.get(5).getPlayerMoney());
      txtPlayer6Assets.setText("$" + players.get(5).getAssets());
      txtPlayer6Total.setText("$" + players.get(5).getTotal());
    }

  }

  /**
   * Sets the players field
   * 
   * @param players
   *          ArrayList of Players to enter
   */
  public void setPlayers(ArrayList<Player> players) {
    this.players = players;
  }

  /**
   * Sets the stockMarket field
   * 
   * @param stockMarket
   *          the Stock Market to enter
   */
  public void setStockMarket(StockMarket stockMarket) {
    this.stockMarket = stockMarket;
  }

  /**
   * Calculates players total assets ie total value of all stock Sets players
   * total assets + money
   */
  public void calculatePlayerAssets() {

    for (Player player : players) {
    int totalPlayerAssets = 0;
      
      totalPlayerAssets = (
          (player.getPlayerSteel() * stockMarket.getStockPrice("steels"))
          + (player.getPlayerShipping() * stockMarket.getStockPrice("shipping"))
          + (player.getPlayerStore() * stockMarket.getStockPrice("stores"))
          + (player.getPlayerMotors() * stockMarket.getStockPrice("motors")));

      player.setAssets(totalPlayerAssets);
      player.setTotal(totalPlayerAssets + player.getPlayerMoney());
    }

  }

  /**
   * Calculates the player with the most money Sets this player as the winner
   */
  public void setWinner() {

    winner = players.get(0);
    for (int n = 0; n < players.size(); n++) {
      if (winner.getTotal() < players.get(n).getTotal()) {
        winner = players.get(n);
      }
    }
  }

  /**
   * Checks if any player has the same total as the winner i.e. tied game
   * 
   * If the game is tied, game winner text is hidden and tied game text is
   * displayed instead
   */
  public void checkForTie() {
    for (Player player : players) {
      if (winner.getTotal() == player.getTotal() && !winner.equals(player)) {
        lblCongrats.setVisible(false);
        lblWinner.setVisible(false);
        lblDraw.setVisible(true);
        lblDraw2.setVisible(true);
      }
    }
  }
}
