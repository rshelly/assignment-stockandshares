/**
 * @author Gavin Fennelly, Alan Walsh, Rob Shelly, Adam Buckley
 * @Version 1.0.0
 */

public class StockMarket {
 
  private int storesShares;
  private int steelsShares;
  private int shippingShares;
  private int motorsShares;
 
  private int storesPrice;
  private int steelsPrice;
  private int shippingPrice;
  private int motorsPrice;
 
  private String stockExchange;
 
  /**
   * Constructor for a stockmarket
   * Creates a stock market with 4 commodities and inital values
   * of 26 available shares in each stock
   */
  public StockMarket() {
    // Setting Default amount of commodities for game Start
    this.storesShares = 28;
    this.steelsShares = 28;
    this.shippingShares = 28;
    this.motorsShares = 28;
 
    // Sets default value of commodities for game Start
    this.storesPrice = 10;
    this.shippingPrice = 10;
    this.motorsPrice = 10;
    this.steelsPrice = 10;
     
    stockExchange = "";
  }
 
  /**
   * Getter for stock exchange display
   * 
   * @return The text that appears in the stock exchange display
   */
  public String getStockExchange() {
    return stockExchange;
  }
 
  /**
   * Adds transactions to the stock exchange text
   * 
   * @param transaction to be added
   */
  public void addToStockExchange(String transaction) {
    stockExchange += transaction;
  }
 
  /**
   * Getter for the number of available shares of any commodity   * 
   * 
   * @param stock The commodity of which number of shares are to be returned
   * @return storeShares, motorsShares, shippingShares, steelShares;
   *  i.e. the number of shares for the given commodity
   */
  public int getStockShares(String stock) {
 
    if (stock.equals("motors")) {
      return motorsShares;
    } else if (stock.equals("shipping")) {
      return shippingShares;
    } else if (stock.equals("stores")) {
      return storesShares;
    } else {
      return steelsShares;
    }
  }
 
  /**
   * Getter for the share price of any commodity
   * 
   * @param stock The commodity of which the share price is to be returned
   * @return storeShares, motorsShares, shippingShares, steelShares;
   * i.e. the share price of the given stock
   */
  public int getStockPrice(String stock) {
 
    if (stock.equals("motors")) {
      return motorsPrice;
    } else if (stock.equals("shipping")) {
      return shippingPrice;
    } else if (stock.equals("stores")) {
      return storesPrice;
    } else {
      return steelsPrice;
    }
  }
 
  /**
   * Method that allows a player to buy stock
   * 
   * @param user The user buying stock
   * @param amount The number of shares the user is buying
   * @param stock The commodity of which the user is purchasing shares
   */
  public void playerBuyStock(int amount, String stock, Player user) {
    //NOTE
    //updatePlayerValues method used for both buying and selling stock
    // therefore when buying the price is multiplied by -1 to make it negative
    // thus it is subtracted from players balance in updatePlayerValues

    //Buy Motors
    if (stock.equals("motors")) {

      motorsShares -= amount;
      //update players balance
      user.updatePlayerValue((amount * motorsPrice * -1), "money");
      //update players stock quantity 
      user.updatePlayerValue(amount, stock);


      //Buy Shipping
    } else if (stock.equals("shipping")) {

      shippingShares -= amount;
      //update players balance
      user.updatePlayerValue(amount * shippingPrice * -1, "money");
      //update players stock quantity
      user.updatePlayerValue(amount, stock);

      //Buy Stores
    } else if (stock.equals("stores")) {
      storesShares -= amount;
      //update players balance
      user.updatePlayerValue(amount * storesPrice * -1, "money");
      //update players stock quantity
      user.updatePlayerValue(amount, stock);

      //Buy Steels
    } else if (stock.equals("steels")) {
      steelsShares -= amount;
      //update players balance
      user.updatePlayerValue(amount * steelsPrice * -1, "money");
      //update players stock quantity
      user.updatePlayerValue(amount, stock);
    }
  }
 
  /**
   * Method that allows a user to sell stock
   * 
   * @param user The user selling stock
   * @param amount The number of shares the user is selling
   * @param stock The commodity of which the user is selling shares
   */
  public void playerSellStock(int amount, String stock, Player user) {

    //NOTE
    //updatePlayerValues method used for both buying and selling stock
    // therefore when selling the amount is multiplied by -1 to make it negative
    // thus it is subtracted from players stock quantity in updatePlayerValues

    //Sell Motors
    if (stock.equals("motors")) {

      motorsShares += amount;
      //update players balance
      user.updatePlayerValue(amount * motorsPrice, "money");
      //update players stock quantity
      user.updatePlayerValue(amount * -1, stock);

      //Sell Shipping  
    } else if (stock.equals("shipping")) {

      shippingShares += amount;
      //update players balance
      user.updatePlayerValue(amount * shippingPrice, "money");
      //update players stock quantity
      user.updatePlayerValue(amount * -1, stock);

      //Sell Stores
    } else if (stock.equals("stores")) {
      storesShares += amount;
      //update players balance
      user.updatePlayerValue(amount * storesPrice, "money");
      //update players stock quantity
      user.updatePlayerValue(amount * -1, stock);

      //Sell Steels
    } else if (stock.equals("steels")) {

      steelsShares += amount;
      //update players balance
      user.updatePlayerValue(amount * steelsPrice, "money");
      //update players stock quantity
      user.updatePlayerValue(amount * -1, stock);
    }
  }
 
  /**
   * Method to check which commodities should be adjusted and calls methods to adjust them accordingly
   * Will adjust all commodities if "all" is passed in stock parameter
   * 
   * @param amount The number by which the number of shares should be adjusted
   * @param stock The stock to be adjusted
   *  can be any of the 4 types of commodity or all commodities
   */
  public void changeShareValues(int amount, String stock) {
 
    if (stock.equals("motors")) {
      // Handles Stock changes for Motors Commodity
      motorsPrice = sharePriceVerification(amount, "motors");
      
    } else if (stock.equals("shipping")) {
      // Handles Shipping price verification and changes
      shippingPrice = sharePriceVerification(amount, "shipping");
      
    } else if (stock.equals("stores")) {
      // Handles Stores price verification and changes
      storesPrice = sharePriceVerification(amount, "stores");
      
    } else if (stock.equals("steels")) {
      // Handles Stock price verification and changes
      steelsPrice = sharePriceVerification(amount, "steels");
      
    } else if (stock.equals("all")) {
      // Handles the Bear & Bull Card stock changes.
      storesPrice = sharePriceVerification(amount, "stores");
      steelsPrice = sharePriceVerification(amount, "steels");
      motorsPrice = sharePriceVerification(amount, "motors");
      shippingPrice = sharePriceVerification(amount, "shipping");
    }
  }
 
  /**
   * Adjusts the number of shares available in a given stock or all stock
   * Limits the number of shares available to [0, 20]
   * 
   * @param amount The value by which the number of shares should be adjusted
   * @param stock The commodity of which the number of shares is to be adjusted
   */
 
  private int sharePriceVerification(int amount, String stock) {
    int price = 0;
    if (stock == "motors") {
      price = motorsPrice;
    }
    if (stock == "shipping") {
      price = shippingPrice;
    }
    if (stock == "stores") {
      price = storesPrice;
    }
    if (stock == "steels") {
      price = steelsPrice;
    }
 
    // verification for all commodity pricing to comply with rules
    if ((price + amount >= 0) && (price + amount <= 20)) {
      price += amount;
    } else if (price + amount < 0) {
      price = 0;
    } else if (price + amount > 20) {
      price = 20;
    }
    return price;
  }
}