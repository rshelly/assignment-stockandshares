import java.awt.EventQueue;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

import javax.swing.JTextArea;

import java.awt.Color;

import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

/**
 * @author  Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version    1.0.0
 */

public class Driver {

  //Declare game objects
  private ArrayList<Player> players;
  private Deck deckOfCards;
  private int roundNum;
  private StockMarket wallstreet;
  private Map<String, JButton> buttonsMap;
  private int activePlayerIndex;
  private int[] oldPrices;

  //Declare players
  private Player player1;
  private Player player2;
  private Player player3;
  private Player player4;
  private Player player5;
  private Player player6;

  //Declare physical contents
  private JMenuBar menuBar;
  private JMenuItem mntmNewGame;
  private JMenuItem mntmExitGame;
  private JMenuItem mntmRules;

  private JFrame frame;
  
  private JLabel lblTitle;
  private JLabel lblNumPlayers;
  private JButton btn3Players;
  private JButton btn4Players;
  private JButton btn5Players;
  private JButton btn6Players;

  private JLabel lblPlayer1Name;
  private JLabel lblPlayer1Balance;
  private JButton btnPlayer1Turn;

  private JLabel lblPlayer2Name;
  private JLabel lblPlayer2Balance;
  private JButton btnPlayer2Turn;

  private JLabel lblPlayer3Name;
  private JLabel lblPlayer3Balance;
  private JButton btnPlayer3Turn;

  private JLabel lblPlayer4Name;
  private JLabel lblPlayer4Balance;
  private JButton btnPlayer4Turn;

  private JLabel lblPlayer5Name;
  private JLabel lblPlayer5Balance;
  private JButton btnPlayer5Turn;

  private JLabel lblPlayer6Name;
  private JLabel lblPlayer6Balance;
  private JButton btnPlayer6Turn;  

  private JButton btnDealCards;  
  private JTextField txtRoundNum;
  private JLabel lblRoundHeader;
  private JTextArea txtStockExchange;
  private JScrollPane scrollPane;

  private JLabel lblMotors;
  private JLabel lblShipping;
  private JLabel lblSteels;
  private JLabel lblStores;
  private JLabel lblMotorsValue;
  private JLabel lblShippingValue;
  private JLabel lblSteelsValue;
  private JLabel lblStoresValue;
  private JLabel lblMotorsIcon;
  private JLabel lblShippingIcon;
  private JLabel lblSteelsIcon;
  private JLabel lblStoresIcon;


  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          Driver window = new Driver();
          window.frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public Driver() {
    
    //Create Players, Deck, StockMarket, Utilities
    newGameObjects();

    //Build Window
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    frame = new JFrame();
    frame.getContentPane().setBackground(new Color(255, 0, 0));
    frame.setBounds(100, 100, 720, 480);
    frame.setSize(720, 480);
    frame.setResizable(false);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().setLayout(null);
    
    //Set some properties for all elements rather than individually
    // Achieved using help from
    // - http://stackoverflow.com/questions/902326/set-property-for-all-child-components
    // - http://thebadprogrammer.com/swing-uimanager-keys/
    
    UIManager.put( "Button.background", (new Color(0, 0, 0)));
    UIManager.put( "Button.foreground", (new Color(255, 255, 255)));
    UIManager.put( "Label.foreground", (new Color(255, 215, 0)));

    //Menu
    
    menuBar = new JMenuBar();
    frame.setJMenuBar(menuBar);
    
    mntmNewGame = new JMenuItem("New Game");
    mntmNewGame.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {       
        resetGame();
      }
    });
    menuBar.add(mntmNewGame);
    
    mntmExitGame = new JMenuItem("Exit Game");
    mntmExitGame.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
         System.exit(0);
      }
    });
    menuBar.add(mntmExitGame);
    
    mntmRules = new JMenuItem("Rules");
    mntmRules.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Rules rules = new Rules();
        rules.main(null);
      }
    });
    menuBar.add(mntmRules);
    
    lblTitle = new JLabel();
    lblTitle.setBounds(258, 5, 200, 105);
    frame.getContentPane().add(lblTitle);
    lblTitle.setVisible(true);
    
    Image title = new ImageIcon(this.getClass().getResource("/stocksandshares.jpg")).getImage();
    lblTitle.setIcon(new ImageIcon(title));
    
    //Number of Players

    lblNumPlayers = new JLabel("Choose Number of Players");
    lblNumPlayers.setFont(new Font("Tahoma", Font.BOLD, 15));
    lblNumPlayers.setHorizontalAlignment(SwingConstants.CENTER);
    lblNumPlayers.setBounds(258, 113, 200, 34);
    frame.getContentPane().add(lblNumPlayers);

    btn3Players = new JButton("");
    btn3Players.setFont(new Font("Tahoma", Font.PLAIN, 8));
    btn3Players.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        players.add(player1);
        players.add(player2);
        players.add(player3);
        enterName();
      }
    });
    btn3Players.setBounds(262, 152, 48, 48);
    frame.getContentPane().add(btn3Players);
    
    Image three = new ImageIcon(this.getClass().getResource("/3.png")).getImage();
    btn3Players.setIcon(new ImageIcon(three));
    btn3Players.setBackground(new Color(255, 0, 0));
    btn3Players.setBorder(new EmptyBorder(0, 0, 0, 0));
    
    btn4Players = new JButton("");
    btn4Players.setFont(new Font("Tahoma", Font.PLAIN, 8));
    btn4Players.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        players.add(player1);
        players.add(player2);
        players.add(player3);
        players.add(player4);
        enterName();
      }
    });
    btn4Players.setBounds(310, 152, 48, 48);
    frame.getContentPane().add(btn4Players);
    
    Image four = new ImageIcon(this.getClass().getResource("/4.png")).getImage();
    btn4Players.setIcon(new ImageIcon(four));
    btn4Players.setBackground(new Color(255, 0, 0));
    btn4Players.setBorder(new EmptyBorder(0, 0, 0, 0));

    btn5Players = new JButton("");
    btn5Players.setFont(new Font("Tahoma", Font.PLAIN, 8));
    btn5Players.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        players.add(player1);
        players.add(player2);
        players.add(player3);
        players.add(player4);
        players.add(player5);
        enterName();
      }
    });
    btn5Players.setBounds(358, 152, 48, 48);
    frame.getContentPane().add(btn5Players);
    
    Image five = new ImageIcon(this.getClass().getResource("/5.png")).getImage();
    btn5Players.setIcon(new ImageIcon(five));
    btn5Players.setBackground(new Color(255, 0, 0));
    btn5Players.setBorder(new EmptyBorder(0, 0, 0, 0));

    btn6Players = new JButton("");
    btn6Players.setFont(new Font("Tahoma", Font.PLAIN, 8));
    btn6Players.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        players.add(player1);
        players.add(player2);
        players.add(player3);
        players.add(player4);
        players.add(player5);
        players.add(player6);
        enterName();
      }
    });
    btn6Players.setBounds(406, 152, 48, 48);
    frame.getContentPane().add(btn6Players);
    
    Image six = new ImageIcon(this.getClass().getResource("/6.png")).getImage();
    btn6Players.setIcon(new ImageIcon(six));
    btn6Players.setBackground(new Color(255, 0, 0));
    btn6Players.setBorder(new EmptyBorder(0, 0, 0, 0));

    //Players

    lblPlayer1Name = new JLabel("");
    lblPlayer1Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer1Name.setBounds(30, 40, 120, 25);
    lblPlayer1Name.setText(player1.getPlayerName());
    frame.getContentPane().add(lblPlayer1Name);

    lblPlayer1Balance = new JLabel("");
    lblPlayer1Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer1Balance.setBounds(30, 60, 100, 20);
    lblPlayer1Balance.setText("$" + player1.getPlayerMoney());
    frame.getContentPane().add(lblPlayer1Balance);

    lblPlayer2Name = new JLabel("");
    lblPlayer2Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer2Name.setText(player2.getPlayerName());
    lblPlayer2Name.setBounds(30, 100, 120, 25);
    frame.getContentPane().add(lblPlayer2Name);

    lblPlayer2Balance = new JLabel("");
    lblPlayer2Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer2Balance.setText("$" + player2.getPlayerMoney());
    lblPlayer2Balance.setBounds(30, 120, 100, 20);
    frame.getContentPane().add(lblPlayer2Balance);

    lblPlayer3Name = new JLabel("");
    lblPlayer3Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer3Name.setText(player3.getPlayerName());
    lblPlayer3Name.setBounds(30, 160, 120, 25);
    frame.getContentPane().add(lblPlayer3Name);

    lblPlayer3Balance = new JLabel("");
    lblPlayer3Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer3Balance.setText("$" + player3.getPlayerMoney());
    lblPlayer3Balance.setBounds(30, 180, 100, 20);
    frame.getContentPane().add(lblPlayer3Balance);

    lblPlayer4Name = new JLabel("");
    lblPlayer4Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer4Name.setText(player4.getPlayerName());
    lblPlayer4Name.setBounds(30, 220, 120, 25);
    frame.getContentPane().add(lblPlayer4Name);
    lblPlayer4Name.setVisible(false);

    lblPlayer4Balance = new JLabel("");
    lblPlayer4Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer4Balance.setText("$" + player4.getPlayerMoney());
    lblPlayer4Balance.setBounds(30, 240, 100, 20);
    frame.getContentPane().add(lblPlayer4Balance);
    lblPlayer4Balance.setVisible(false);

    lblPlayer5Name = new JLabel("");
    lblPlayer5Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer5Name.setText(player5.getPlayerName());
    lblPlayer5Name.setBounds(30, 280, 120, 25);
    frame.getContentPane().add(lblPlayer5Name);
    lblPlayer5Name.setVisible(false);

    lblPlayer5Balance = new JLabel("");
    lblPlayer5Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer5Balance.setText("$" + player5.getPlayerMoney());
    lblPlayer5Balance.setBounds(30, 300, 100, 20);
    frame.getContentPane().add(lblPlayer5Balance);
    lblPlayer5Balance.setVisible(false);

    lblPlayer6Name = new JLabel("");
    lblPlayer6Name.setFont(new Font("Tunga", Font.BOLD, 32));
    lblPlayer6Name.setText(player6.getPlayerName());
    lblPlayer6Name.setBounds(30, 340, 120, 25);
    frame.getContentPane().add(lblPlayer6Name);
    lblPlayer6Name.setVisible(false);

    lblPlayer6Balance = new JLabel("");
    lblPlayer6Balance.setFont(new Font("Tahoma", Font.PLAIN, 20));
    lblPlayer6Balance.setText("$" + player6.getPlayerMoney());
    lblPlayer6Balance.setBounds(30, 360, 100, 20);
    frame.getContentPane().add(lblPlayer6Balance);
    lblPlayer6Balance.setVisible(false);

    //Game Controls
    btnDealCards = new JButton("Deal Cards");
    btnDealCards.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        startRound();
      }
    });
    btnDealCards.setBounds(312, 354, 100, 40);
    frame.getContentPane().add(btnDealCards);

    lblRoundHeader = new JLabel("Round");
    lblRoundHeader.setHorizontalAlignment(SwingConstants.CENTER);
    lblRoundHeader.setFont(new Font("Tahoma", Font.PLAIN, 32));
    lblRoundHeader.setBounds(285, 200, 150, 30);
    frame.getContentPane().add(lblRoundHeader);

    txtRoundNum = new JTextField("");
    txtRoundNum.setHorizontalAlignment(SwingConstants.CENTER);
    txtRoundNum.setBackground(new Color(255, 0, 0));
    txtRoundNum.setForeground(new Color(255, 215, 0));
    txtRoundNum.setBorder(new EmptyBorder(0, 0, 0, 0));
    txtRoundNum.setFont(new Font("Tahoma", Font.PLAIN, 64));
    txtRoundNum.setBounds(285, 230, 150, 75);
    txtRoundNum.setText("" + roundNum);
    txtRoundNum.setEnabled(false);
    frame.getContentPane().add(txtRoundNum);

    //Stock Market

    lblMotors = new JLabel("Motors");
    lblMotors.setFont(new Font("Tahoma", Font.BOLD, 14));
    lblMotors.setHorizontalAlignment(SwingConstants.CENTER);
    lblMotors.setBounds(464, 60, 60, 15);
    frame.getContentPane().add(lblMotors);

    lblMotorsValue = new JLabel("");
    lblMotorsValue.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblMotorsValue.setHorizontalAlignment(SwingConstants.CENTER);
    lblMotorsValue.setBounds(464, 85, 40, 15);
    lblMotorsValue.setText("$" + wallstreet.getStockPrice("motors"));
    frame.getContentPane().add(lblMotorsValue);

    lblShipping = new JLabel("Shipping");
    lblShipping.setFont(new Font("Tahoma", Font.BOLD, 14));
    lblShipping.setHorizontalAlignment(SwingConstants.CENTER);
    lblShipping.setBounds(524, 60, 60, 15);
    frame.getContentPane().add(lblShipping);

    lblShippingValue = new JLabel("");
    lblShippingValue.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblShippingValue.setHorizontalAlignment(SwingConstants.CENTER);
    lblShippingValue.setBounds(524, 85, 40, 15);
    lblShippingValue.setText("$" + wallstreet.getStockPrice("shipping"));
    frame.getContentPane().add(lblShippingValue);

    lblSteels = new JLabel("Steels");
    lblSteels.setFont(new Font("Tahoma", Font.BOLD, 14));
    lblSteels.setHorizontalAlignment(SwingConstants.CENTER);
    lblSteels.setBounds(584, 60, 60, 15);
    frame.getContentPane().add(lblSteels);

    lblSteelsValue = new JLabel("");
    lblSteelsValue.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblSteelsValue.setHorizontalAlignment(SwingConstants.CENTER);
    lblSteelsValue.setBounds(584, 85, 40, 15);
    lblSteelsValue.setText("$" + wallstreet.getStockPrice("steels"));
    frame.getContentPane().add(lblSteelsValue);

    lblStores = new JLabel("Stores");
    lblStores.setFont(new Font("Tahoma", Font.BOLD, 14));
    lblStores.setHorizontalAlignment(SwingConstants.CENTER);
    lblStores.setBounds(644, 60, 60, 15);
    frame.getContentPane().add(lblStores);

    lblStoresValue = new JLabel("");
    lblStoresValue.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblStoresValue.setHorizontalAlignment(SwingConstants.CENTER);
    lblStoresValue.setBounds(644, 85, 40, 15);
    lblStoresValue.setText("$" + wallstreet.getStockPrice("stores"));
    frame.getContentPane().add(lblStoresValue);
    
    lblMotorsIcon = new JLabel("");
    lblMotorsIcon.setBounds(504, 85, 16, 16);
    frame.getContentPane().add(lblMotorsIcon);
    
    lblShippingIcon = new JLabel("");
    lblShippingIcon.setBounds(564, 85, 16, 16);
    frame.getContentPane().add(lblShippingIcon);
    
    lblSteelsIcon = new JLabel("");
    lblSteelsIcon.setBounds(624, 85, 16, 16);
    frame.getContentPane().add(lblSteelsIcon);
    
    lblStoresIcon = new JLabel("");
    lblStoresIcon.setBounds(684, 85, 16, 16);
    frame.getContentPane().add(lblStoresIcon);
    
    
    //Create Icons for labels
    //Achieved using help from
    // - https://www.youtube.com/watch?v=tFwp589MAFk
    Image up = new ImageIcon(this.getClass().getResource("/up.png")).getImage();
    Image down = new ImageIcon(this.getClass().getResource("/down.png")).getImage();
    
    txtStockExchange = new JTextArea();
    txtStockExchange.setForeground(Color.GREEN);
    txtStockExchange.setBackground(Color.BLACK);
    txtStockExchange.setBorder(new EmptyBorder(0,0,0,0));
    txtStockExchange.setBounds(455, 110, 250, 310);
    txtStockExchange.setEditable(false);
    txtStockExchange.setText(wallstreet.getStockExchange());
    frame.getContentPane().add(txtStockExchange);


    scrollPane = new JScrollPane(txtStockExchange);
    scrollPane.setBounds(455, 110, 250, 310);
    frame.getContentPane().add(scrollPane);

    //Play buttons

    btnPlayer1Turn = new JButton("Play");
    btnPlayer1Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        playTurn(player1);
        nextPlayer();

      }
    });
    btnPlayer1Turn.setBounds(159, 50, 89, 23);
    frame.getContentPane().add(btnPlayer1Turn);
    btnPlayer1Turn.setVisible(false);

    btnPlayer2Turn = new JButton("Play");
    btnPlayer2Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        playTurn(player2);
        nextPlayer();

      }
    });
    btnPlayer2Turn.setBounds(160, 110, 89, 23);
    frame.getContentPane().add(btnPlayer2Turn);
    btnPlayer2Turn.setVisible(false);

    btnPlayer3Turn = new JButton("Play");
    btnPlayer3Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        playTurn(player3);
        nextPlayer();

      }
    });
    btnPlayer3Turn.setBounds(160, 170, 89, 23);
    frame.getContentPane().add(btnPlayer3Turn);
    btnPlayer3Turn.setVisible(false);

    btnPlayer4Turn = new JButton("Play");
    btnPlayer4Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        playTurn(player4);
        nextPlayer();

      }
    });
    btnPlayer4Turn.setBounds(160, 230, 89, 23);
    frame.getContentPane().add(btnPlayer4Turn);
    btnPlayer4Turn.setVisible(false);

    btnPlayer5Turn = new JButton("Play");
    btnPlayer5Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        playTurn(player5);
        nextPlayer();

      }
    });
    btnPlayer5Turn.setBounds(160, 290, 89, 23);
    frame.getContentPane().add(btnPlayer5Turn);
    btnPlayer5Turn.setVisible(false);

    btnPlayer6Turn = new JButton("Play");
    btnPlayer6Turn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        playTurn(player6);
        nextPlayer();

      }
    });
    btnPlayer6Turn.setBounds(160, 350, 89, 23);
    frame.getContentPane().add(btnPlayer6Turn);
    btnPlayer6Turn.setVisible(false);
    
    //set visibility of all controls when window is constructed
    //NOTE Players 4,5,6 when constructing window are set not visible
    //this is because setGamePlayControls only targets the playing players
    setNumPlayerControls(true);
    setGamePlayControls(false, false);    
  }

  /**
   * Loops through all players
   * Prompting them to enter their name
   */
  private void enterName() {
    //Variable i is created as a variable representing player indexes
    //so they can be passed to the EnterName dialog and prompt players
    //to enter names appropriately
    int i = 0;
    for (Player player: players) {
      EnterName enterName = new EnterName(players, (i));
      enterName.main(players, i);
      i++;
    }

    lblPlayer1Name.setText(player1.getPlayerName());
    lblPlayer2Name.setText(player2.getPlayerName());
    lblPlayer3Name.setText(player3.getPlayerName());
    lblPlayer4Name.setText(player4.getPlayerName());
    lblPlayer5Name.setText(player5.getPlayerName());
    lblPlayer6Name.setText(player6.getPlayerName());

    startGame();
  }

  /**
   * Sets up a game after players have been selected
   * by making all the necessary components visible
   */
  private void startGame() {

    //Fill HashMap for Buttons
    fillHashMap();      

    //Remove controls for selecting players
    setNumPlayerControls(false);

    //Add Game Elements    
    setGamePlayControls(true, false);
  }

  /**
   * Start a round
   * Set the first player for the round as active player.
   * Makes button visible for active player to play turn
   */
  private void startRound() {

    btnDealCards.setVisible(false);
    dealCards();
    activePlayerIndex = 0;
    buttonsMap.get(players.get(activePlayerIndex).getPlayerName()).setVisible(true);
  }

  /**
   * Shuffles the deck and deals cards from the top to each player
   */
  public void dealCards()
  {
    ArrayList<Card> deck = deckOfCards.getDeck();
    Collections.shuffle(deck);

    int i = 0;
    for (Player player: players) {
      player.setCard(deck.get(i));
      i++;
    }
  }

  /**
   * Moves the round onto the next player to take their turn
   * Hides the button for the previous player
   * Makes button visible for next player to play turn
   */
  private void nextPlayer() {

    //Hide Play button for previous player
    buttonsMap.get(players.get(activePlayerIndex).getPlayerName()).setVisible(false);

    //Running check before actually incrementing activePlayerIndex
    //ensures that it won't be incremented if it would go out of bounds
    //Thus the active player's button can still be disabled in endRound method
    if (activePlayerIndex  + 1 < players.size()) {
      activePlayerIndex++;
      //Show button for next player
      buttonsMap.get(players.get(activePlayerIndex).getPlayerName()).setVisible(true);
    } else {
      endRound();
    }
  }

  /**
   * Ends a round
   * Hides the button for the previous player
   * Moves onto next round or end the game if round ending is last round
   * Adjusts the order in which players will take turns in next round
   * 
   */
  private void endRound() {
    buttonsMap.get(players.get(activePlayerIndex).getPlayerName()).setVisible(false);
    roundNum++;
    txtRoundNum.setText("" +roundNum);

    //adjust players order in Arraylist
    //thus changing the order in which players take their turn
    Collections.rotate(players, -1);

    evaluateCards();
    updateSharePrices();

    if (roundNum < 13 ) {

      btnDealCards.setVisible(true);
      
    } else {
      endGame();
    }
  }

  /**
   * Ends Game
   * Opens a new window that ends the game
   */
  private void endGame() {
    GameEnd gameEnd  = new GameEnd(players, wallstreet);
    gameEnd.main(players, wallstreet);

    //This will reset game the game
    //This OK because this  code will only run if the player chooses play again
    // in Game End Dialog
    resetGame();
  }

  /**
   * Open window for a player to take their turn
   * i.e. view their card, buy/sell stock
   * 
   * @param player The player who will take their turn
   */
  private void playTurn(Player player) {

    //reset players transaction (player has not made any transaction this round)
    player.resetPlayerTransactions();

    //open playerTurn dialog box      
    PlayerTurn playerTurn = new PlayerTurn(player, wallstreet, oldPrices);    
    playerTurn.main(player, wallstreet, oldPrices);

    //add players transactions to stock exchange
    wallstreet.addToStockExchange(player.getPlayerTransactions());
    //update stock exchange display
    txtStockExchange.setText(wallstreet.getStockExchange());
    //update players balance display
    updatePlayerBalances();
  }

  /**
   * Fills a hash map of String to JButton
   * Allows buttons to be activated using Strings
   *  i.e. Activate a player's button using the player's name
   */
  private void fillHashMap() {
    buttonsMap.put(player1.getPlayerName(), btnPlayer1Turn);
    buttonsMap.put(player2.getPlayerName(), btnPlayer2Turn);
    buttonsMap.put(player3.getPlayerName(), btnPlayer3Turn);
    buttonsMap.put(player4.getPlayerName(), btnPlayer4Turn);
    buttonsMap.put(player5.getPlayerName(), btnPlayer5Turn);
    buttonsMap.put(player6.getPlayerName(), btnPlayer6Turn);
  }
  
  /**
   * Updates the display of the balances of all players
   */
  public void updatePlayerBalances() {
    lblPlayer1Balance.setText("$" + player1.getPlayerMoney());
    lblPlayer2Balance.setText("$" + player2.getPlayerMoney());
    lblPlayer3Balance.setText("$" + player3.getPlayerMoney());
    lblPlayer4Balance.setText("$" + player4.getPlayerMoney());
    lblPlayer5Balance.setText("$" + player5.getPlayerMoney());
    lblPlayer6Balance.setText("$" + player6.getPlayerMoney());
  }
  
  /**
   * Updates the display of the share prices
   */
  public void updateSharePrices() {
    lblMotorsValue.setText("$" + wallstreet.getStockPrice("motors"));
    lblShippingValue.setText("$" + wallstreet.getStockPrice("shipping"));
    lblSteelsValue.setText("$" + wallstreet.getStockPrice("steels"));
    lblStoresValue.setText("$" + wallstreet.getStockPrice("stores"));
  }
  
  /**
   * Evaluates each players cards at the end of each round
   * Adjust stock prices accordingly 
   * Adjusts stock icons accordingly
   */
  public void evaluateCards() {
    
    //Record old share prices into list
    setOldSharePrices();
    
    //Evaluate cards and adjust share prices accordingly
    for (Player player: players) {
      Card card = player.getCard();
      wallstreet.changeShareValues(card.getValue(), card.getStock());
    }
    
    //Create Icons for labels
    //Achieved using help from
    // - https://www.youtube.com/watch?v=tFwp589MAFk
    Image up = new ImageIcon(this.getClass().getResource("/up.png")).getImage();
    Image down = new ImageIcon(this.getClass().getResource("/down.png")).getImage();
    
    //Evaluate total change in share prices
    //and adjust icons accordingly
    if (oldPrices[0] < wallstreet.getStockPrice("motors")) {
      lblMotorsIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[0] > wallstreet.getStockPrice("motors")) {
      lblMotorsIcon.setIcon(new ImageIcon(down));
    } else { //if old price and current price are equal, don't show any arrow
      lblMotorsIcon.setIcon(null);
    }
    
    if (oldPrices[1] < wallstreet.getStockPrice("shipping")) {
      lblShippingIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[1] > wallstreet.getStockPrice("shipping")) {
      lblShippingIcon.setIcon(new ImageIcon(down));
    } else {
      lblShippingIcon.setIcon(null);
    }
    
    if (oldPrices[2] < wallstreet.getStockPrice("steels")) {
      lblSteelsIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[2] > wallstreet.getStockPrice("steels")) {
      lblSteelsIcon.setIcon(new ImageIcon(down));
    } else {
      lblSteelsIcon.setIcon(null);
    }
    
    if (oldPrices[3] < wallstreet.getStockPrice("stores")) {
      lblStoresIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[3] > wallstreet.getStockPrice("stores")) {
      lblStoresIcon.setIcon(new ImageIcon(down));
    } else {
      lblStoresIcon.setIcon(null);
    }
  }
  
  /**
   * Records all share prices into a primitive array.
   * Used to store previous shares prices
   * 
   * @return Primitive array of all share prices
   */
  public void setOldSharePrices () {
    oldPrices[0] = wallstreet.getStockPrice("motors");
    oldPrices[1] = wallstreet.getStockPrice("shipping");
    oldPrices[2] = wallstreet.getStockPrice("steels");
    oldPrices[3] = wallstreet.getStockPrice("stores");
  }

  /**
   * Reset Game to initial state to play again
   * 
   * Resets all players to initial values
   * Sets number of players options visible
   * 
   */
  private void resetGame() {
    
    newGameObjects();
    
    setNumPlayerControls(true);
    setGamePlayControls(false, true);
    
    lblMotorsIcon.setIcon(null);
    lblShippingIcon.setIcon(null);
    lblSteelsIcon.setIcon(null);
    lblStoresIcon.setIcon(null);
    
    txtStockExchange.setText("");
    txtRoundNum.setText("" + roundNum);
    updatePlayerBalances();
    updateSharePrices();
  }
  
  /**
   * Reconstructs game objects
   * i.e. players, deck, stockmarket, round number etc.
   * 
   * Used to return all fields to initial state to start new game
   */
  public void newGameObjects() {
    player1 = new Player("Player1");
    player2 = new Player("Player2");
    player3 = new Player("Player3");
    player4 = new Player("Player4");
    player5 = new Player("Player5");
    player6 = new Player("Player6");
    
    players = new ArrayList<Player>();
    deckOfCards = new Deck();
    wallstreet = new StockMarket();
    roundNum = 1;
    buttonsMap = new HashMap<>();
    activePlayerIndex = 0;
    oldPrices = new int[4];
    oldPrices[0] = 10;
    oldPrices[1] = 10;
    oldPrices[2] = 10;
    oldPrices[3] = 10;
  }

  /**
   * Setter for visibility of initial game controls
   * i.e. buttons to choose the number of players
   * 
   * @param visible Boolean that determines if objects are visible (true) or not (false)
   */
  public void setNumPlayerControls(boolean visible) {
    lblNumPlayers.setVisible(visible);
    btn3Players.setVisible(visible);
    btn4Players.setVisible(visible);
    btn5Players.setVisible(visible);
    btn6Players.setVisible(visible);
  }
  
  /**
   * Setter for visibility of main game play controls
   * i.e. players, commodities, stock exchange
   * 
   * NOTE: Method only targets player controls for player that are playing
   * e.g. if there are only 3 players, controls or players 4,5,6 are not targeted
   * Thus, initial visibility must be set for these players on build
   * 
   * @param visible Boolean determines if objects are visible (true) or not (false)
   * @param resetGame Boolean dictates if method is being called on a reset game
   *  - if game is being reset, all player's play buttons must be ensured to be not visible
   */
  public void setGamePlayControls(boolean visible, boolean resetGame) {    
    lblPlayer1Name.setVisible(visible);
    lblPlayer1Balance.setVisible(visible);
    lblPlayer2Name.setVisible(visible);
    lblPlayer2Balance.setVisible(visible);
    lblPlayer3Name.setVisible(visible);
    lblPlayer3Balance.setVisible(visible);
    if ( (players.size() >= 4) || resetGame ) {
      lblPlayer4Name.setVisible(visible);
      lblPlayer4Balance.setVisible(visible);
    }
    if ( (players.size() >= 5) || resetGame ) {
      lblPlayer5Name.setVisible(visible);
      lblPlayer5Balance.setVisible(visible);
    }
    if ( (players.size() >= 6) || resetGame ) {
      lblPlayer6Name.setVisible(visible);
      lblPlayer6Balance.setVisible(visible);
    }
    
    if (resetGame) {
      btnPlayer1Turn.setVisible(false);
      btnPlayer2Turn.setVisible(false);
      btnPlayer3Turn.setVisible(false);
      btnPlayer4Turn.setVisible(false);
      btnPlayer5Turn.setVisible(false);
      btnPlayer6Turn.setVisible(false);
    }

    btnDealCards.setVisible(visible);
    lblRoundHeader.setVisible(visible);
    txtRoundNum.setVisible(visible);

    lblMotors.setVisible(visible);
    lblMotorsValue.setVisible(visible);
    lblShipping.setVisible(visible);
    lblShippingValue.setVisible(visible);
    lblSteels.setVisible(visible);
    lblSteelsValue.setVisible(visible);
    lblStores.setVisible(visible);
    lblStoresValue.setVisible(visible);
    lblMotorsIcon.setVisible(visible);
    lblShippingIcon.setVisible(visible);
    lblStoresIcon.setVisible(visible);
    lblSteelsIcon.setVisible(visible);
    txtStockExchange.setVisible(visible);
    scrollPane.setVisible(visible);
  }
  
  /** Method allows easy printing to stock exchange from driver
   *  Used only for on screen testing, never during game play
   * @param print
   */
  private void print(String print) {
    wallstreet.addToStockExchange(print);
    txtStockExchange.setText(wallstreet.getStockExchange());
  }
}