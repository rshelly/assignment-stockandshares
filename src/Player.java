/**
 * @author Gavin Fennelly, Alan Walsh, Rob Shelly, Adam Buckley
 * @Version 1.0.0
 */

public class Player

{
  private String name;
  private int money;
  private int steelShares;
  private int shippingShares;
  private int storesShares;
  private int motorsShares;
  private Card card;
  private int assets;
  private int total;
  private String transactions;

  /**
   * Constructor for a Player
   * Creates a player with start of game values for all fields
   * @param The players name
   */
  public Player(String name) {
    // Sets Player name
    this.name = name;

    reset();
  }

  /**
   * Getter for the player's name
   * 
   * @return the name of the player
   */
  public String getPlayerName() {
    return name;
  }

  /**
   * Sets the player's name
   * 
   * @param name
   *          The player's name
   */
  public void setPlayerName(String name) {
    this.name = name;
  }

  /**
   * Getter for the player's money
   * 
   * @return the money the player currently has
   */
  public int getPlayerMoney() {
    return money;
  }

  /**
   * Getter for the player's Steels stock
   * 
   * @return the amount of shares owned in steel commodity
   */
  public int getPlayerSteel() {
    return steelShares;
  }

  /**
   * Getter for the player's Shipping stock
   * 
   * @return the amount of shares owned in shipping commodity
   */
  public int getPlayerShipping() {
    return shippingShares;
  }

  /**
   * Getter for the player's Stores stock
   * 
   * @return the amount of shares owned in stores commodity
   */
  public int getPlayerStore() {
    return storesShares;
  }

  /**
   * Getter for the player's Motors stock
   * 
   * @return the amount of shares owned in motors commodity
   */
  public int getPlayerMotors() {
    return motorsShares;
  }

  /**
   * Gets the players total value of all stocks
   * 
   * @return the players assets
   */
  public int getAssets() {
    return assets;
  }

  /**
   * Setter for the players Assets
   * 
   * @param assets
   *          The total value of all the player's stock
   */
  public void setAssets(int assets) {
    this.assets = assets;
  }

  /**
   * Get the players total money plus assets
   * 
   * @return the players total money plus assets
   */
  public int getTotal() {
    return total;
  }

  /**
   * Setter for the players total
   *
   * @param total
   *          The player's assets plus money
   */
  public void setTotal(int total) {
    this.total = total;
  }

  /**
   * Getter for the card dealt to a player
   * 
   * @return The player's card
   */
  public Card getCard() {
    return card;
  }

  /**
   * Setter for the player's card. Allows card to be dealt to player
   * 
   * @param the
   *          card dealt to the player
   */
  public void setCard(Card card) {
    this.card = card;
  }

  /**
   * Gets the transactions a player made during their turn Is used to send
   * players trading to stock exchange display after their turn
   * 
   * @return The list of transactions a player made on current/last turn
   */
  public String getPlayerTransactions() {
    return transactions;
  }

  /**
   * Resets players transaction to start new turn Sets it so "" instead of null
   * to avoid null appearing on when printed to display
   */
  public void resetPlayerTransactions() {
    transactions = ("");
  }

  /**
   * Add a transaction to the players list of transaction for turn
   * 
   * @param transaction
   *          The transaction to be added
   */
  public void addPlayerTransaction(String transaction) {
    transactions += transaction;
  }

  /**
   * Updates player's fields Can be used to update all stock types of money
   * 
   * @param amount
   *          The quantity the field should be adjusted
   * @param value
   *          The field to be updated
   */
  public void updatePlayerValue(int amount, String value) {
    // Changes String to lower case characters & trims excess spaces on end of
    // strings
    // don't think this is neccessary because value comes in a a parameter from
    // StockMarket
    // but it has already been timmed and lowercased in StockMarket
    value = value.toLowerCase();
    value = value.trim();

    if (value.equals("money")) {
      money += amount;
    } else if (value.equals("steel") || (value.equals("steels"))) {
      steelShares += amount;
    } else if (value.equals("shipping")) {
      shippingShares += amount;
    } else if (value.equals("store") || (value.equals("stores"))) {
      storesShares += amount;
    } else if (value.equals("motors") || (value.equals("motor"))) {
      motorsShares += amount;
    }
  }

  /**
   * A getter for the shares a player has for a given commodity Allows shares of
   * any commodity to be returned by passing the required commodity as a
   * parameter
   * 
   * @param stock
   *          The commodity of which shares should be returned
   * @return The number of shares the player possesses of the given commodity
   */
  public int getAnyShares(String stock) {
    if (stock.equals("motors")) {
      return getPlayerMotors();
    } else if (stock.equals("shipping")) {
      return getPlayerShipping();
    } else if (stock.equals("steels")) {
      return getPlayerSteel();
    } else {
      return getPlayerStore();
    }
  }

  /**
   * Sets all player fields start of game values Used when constructing players
   * at start of game or when starting new game to reset players
   */
  public void reset() {

    // Sets default amount of money and shares for the game start
    money = 80;
    steelShares = 0;
    shippingShares = 0;
    storesShares = 0;
    motorsShares = 0;
    assets = 0;
    total = 80;

    // Creates a place to store the card which will be dealt to players each
    // turn
    card = null;

  }
}