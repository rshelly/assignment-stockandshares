import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;
import java.awt.Font;

public class Rules extends JDialog {

	private JButton btnExit;
	private JButton btnNextPage;
	private JButton btnPreviousPage;
	private JTextArea textArea;
	private String RulesPage1;
	private String RulesPage2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Rules dialog = new Rules();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Rules() {
		setTitle("Stocks & Shares: Game Rules");
		setBounds(100, 100, 600, 300);
		getContentPane().setLayout(null);

		RulesPage1 = "Stocks and Shares Rules:\n\n1) Players with no money or shares must\nimmediatly quit the game.\n"
				+ "2) Shares cannot be increased over $20.\n"
				+ "3) Shares cannot be bought or sold at $0.\n"
				+ "4) Each players starts with $80.\n"
				+ "5) Each type of share is valued at $10 at game start.\n"
				+ "6) The winner is the player who ends the game with the most money.\n"
				+ "7) There are four different kinds of shares:\nsteels, motors, shipping and stores."
				;
		RulesPage2 = "8) Each share type has six different cards: up $2, up $3, up $4,\ndown $2, down $3 and down $4.\n"
				+ "9) The special bear card causes all share prices to drop by $4.\n"
				+ "10) The special bull card causes all share prices to increase by $4.";

		btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnExit.setBounds(335, 227, 89, 23);
		getContentPane().add(btnExit);

		btnNextPage = new JButton("Next Page");
		btnNextPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText(RulesPage2);

			}
		});
		btnNextPage.setBounds(223, 227, 93, 23);
		getContentPane().add(btnNextPage);

		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea.setBounds(10, 11, 550, 200);
		getContentPane().add(textArea);
		textArea.setColumns(10);
		textArea.setText(RulesPage1);

		btnPreviousPage = new JButton("Previous page");
		btnPreviousPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText(RulesPage1);

			}
		});
		btnPreviousPage.setBounds(71, 227, 142, 23);
		getContentPane().add(btnPreviousPage);	
	}
}