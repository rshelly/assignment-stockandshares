import java.awt.Image;

/**
 * @author  Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version    1.0.0
 */
 
public class Card {
     
    private String stock;
    private int value;
    private Image card;

    /**
     * Constructor for Card.
     * @param stock the type of stock that the card is
     * @param value the value of which the card states
     * @param card the graphical image of the card
     */
    public Card(String stock, int value, Image card)
    {
        this.stock = stock;
        this.value = value;
        this.card = card;
    }

    /**
     * A String representation is returned of the card's specific details.
     * @return details of card in question
     */
    public String toString()
    {
        return "The type of stock is " + stock + "\n and the value of "
        + stock + " stock is: " + value;
    }

    /**
     * Getter for the card's stock type
     * 
     * @returns the card's stock type
     */
    public  String  getStock()
    {
        return stock;
    }

    /**
     * Getter for card's value
     * 
     * @return the card's value
     */
    public  int  getValue()
    {
        return value;
    }
    
    /** Gets a graphical representation of the card
     * @return The image of the card
     */
    public Image getCard() {
      return card;
    }
    
  /**
   * Sets the card's stock type
   * 
   * @param stock
   *          The stock type
   */
  public void setStock(String stock) {
    this.stock = stock;
  }

  /**
   * Sets the card's value
   * 
   * @param value
   *          The value
   */
  public void setValue(int value) {
    this.value = value;
  }     
}