import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.SwingConstants;

/**
 * @author  Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version    1.0.0
 */

public class PlayerTurn extends JDialog {
  
  //Declare game objects
  private Player player;
  private StockMarket stockMarket;
  private String commodity;
  private int[] oldPrices;
  
  //Declare physical contents
  private JLabel lblPlayerName;
  private JButton btnExit;
  private JButton btnBuy;
  private JButton btnSell;
  
  private int motorsBought;
  private int shippingBought;
  private int steelsBought;
  private int storesBought;     

  private JLabel lblQuantity;
  private JTextField txtQuantity;
  private JTextField txtBalance;
  
  private JLabel lblMotors;
  private JLabel lblShipping;
  private JLabel lblSteels;
  private JLabel lblStores;
  private JTextField txtMotorsShares;
  private JTextField txtShippingShares;
  private JTextField txtSteelsShares;
  private JTextField txtStoresShares;
  private JTextField txtMotorsPrice;
  private JTextField txtShippingPrice;
  private JTextField txtSteelsPrice;
  private JTextField txtStoresPrice;
  private JLabel lblCard;
  private JComboBox cbxCommodities;

  private JLabel lblMotorsIcon;
  private JLabel lblShippingIcon;
  private JLabel lblSteelsIcon;
  private JLabel lblStoresIcon;


  /**
   * Launches the dialog box
   * Takes in the current player and the stock market as parameters
   * Calls the constructor to build the dialog box
   * 
   * @param player The current player
   * @param stockMarket The stock market with current share prices
   */
  public static void main(Player player, StockMarket stockMarket, int[] oldPrices) {
    try {
      PlayerTurn dialog = new PlayerTurn(player, stockMarket, oldPrices);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      //Sets the Dialog Box's modality to application
      //This means parent window will pause and will not accept user interaction
      //until Dialog Box has closed
      dialog.setModalityType(ModalityType.APPLICATION_MODAL);
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Builds the dialog box
   * Takes in the current player and the stock market as parameters
   *  and sets them in the class fields so they can be used in all methods
   * 
   * @param player The current player
   * @param stockMarket The stock market with current share prices
   */
  public PlayerTurn(Player player, StockMarket stockMarket, int[] oldPrices) {
    getContentPane().setBackground(Color.RED);

    setBounds(150, 150, 640, 480);
    getContentPane().setLayout(null);
    
    UIManager.put( "TextField.background", (new Color(255, 0, 0)));
    UIManager.put( "TextField.foreground", (new Color(255, 215, 0)));
    UIManager.put( "TextField.border", (new EmptyBorder(0, 0, 0, 0)));
    UIManager.put( "Label.font", (new Font("Tahoma", Font.PLAIN, 20)));
    
    //set the parameters passed to Dialog Box as global variables for dialog
    //so they can accessed in all methods
    setPlayer(player);
    setStockMarket(stockMarket);
    setOldPrices(oldPrices);
    
    lblPlayerName = new JLabel("New label");
    lblPlayerName.setFont(new Font("Tahoma", Font.PLAIN, 32));
    lblPlayerName.setBounds(20, 20, 186, 35);
    getContentPane().add(lblPlayerName);
    lblPlayerName.setText(player.getPlayerName());

    btnBuy = new JButton("Buy");
    btnBuy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        buy();
        setValues();

      }
    });
    btnBuy.setBounds(20, 273, 80, 25);
    getContentPane().add(btnBuy);

    btnSell = new JButton("Sell");
    btnSell.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        sell();
        setValues();

      }
    });
    btnSell.setBounds(20, 317, 80, 25);
    getContentPane().add(btnSell);

    txtQuantity = new JTextField();
    txtQuantity.setHorizontalAlignment(SwingConstants.RIGHT);
    txtQuantity.setBounds(252, 319, 40, 20);
    getContentPane().add(txtQuantity);
    txtQuantity.setColumns(10);
    txtQuantity.setBackground(new Color(255, 255, 255));
    txtQuantity.setForeground(new Color(0, 0, 0));

    txtBalance = new JTextField(player.getPlayerMoney());
    txtBalance.setHorizontalAlignment(SwingConstants.RIGHT);
    txtBalance.setFont(new Font("Tahoma", Font.PLAIN, 32));
    txtBalance.setBounds(215, 20, 75, 35);
    txtBalance.setEditable(false);
    getContentPane().add(txtBalance);
    txtBalance.setColumns(10);
    txtBalance.setText("$" + getPlayer().getPlayerMoney());
    
    lblMotors = new JLabel("Motors");
    lblMotors.setBounds(20, 100, 100, 20);
    getContentPane().add(lblMotors);
    
    lblShipping = new JLabel("Shipping");
    lblShipping.setBounds(20, 130, 100, 20);
    getContentPane().add(lblShipping);
    
    lblSteels = new JLabel("Steels");
    lblSteels.setBounds(20, 160, 100, 20);
    getContentPane().add(lblSteels);
    
    lblStores = new JLabel("Stores");
    lblStores.setBounds(20, 190, 100, 20);
    getContentPane().add(lblStores);
    
    txtMotorsShares = new JTextField();
    txtMotorsShares.setBounds(150, 100, 50, 20);
    getContentPane().add(txtMotorsShares);
    txtMotorsShares.setColumns(10);
    txtMotorsShares.setEnabled(false);
    
    
    txtShippingShares = new JTextField();
    txtShippingShares.setBounds(150, 130, 50, 20);
    getContentPane().add(txtShippingShares);
    txtShippingShares.setColumns(10);
    txtShippingShares.setEnabled(false);
    
    txtSteelsShares = new JTextField();
    txtSteelsShares.setBounds(150, 160, 50, 20);
    getContentPane().add(txtSteelsShares);
    txtSteelsShares.setColumns(10);
    txtSteelsShares.setEnabled(false);
    
    txtStoresShares = new JTextField();
    txtStoresShares.setBounds(150, 190, 50, 20);
    getContentPane().add(txtStoresShares);
    txtStoresShares.setColumns(10);
    txtStoresShares.setEnabled(false);
    
    txtMotorsPrice = new JTextField();
    txtMotorsPrice.setBounds(210, 100, 50, 20);
    getContentPane().add(txtMotorsPrice);
    txtMotorsPrice.setColumns(10);
    txtMotorsPrice.setEnabled(false);
    
    txtShippingPrice = new JTextField();
    txtShippingPrice.setBounds(210, 130, 50, 20);
    getContentPane().add(txtShippingPrice);
    txtShippingPrice.setColumns(10);
    txtShippingPrice.setEnabled(false);
    
    txtSteelsPrice = new JTextField();
    txtSteelsPrice.setBounds(210, 160, 50, 20);
    getContentPane().add(txtSteelsPrice);
    txtSteelsPrice.setColumns(10);
    txtSteelsPrice.setEnabled(false);
    
    txtStoresPrice = new JTextField();
    txtStoresPrice.setBounds(210, 190, 50, 20);
    getContentPane().add(txtStoresPrice);
    txtStoresPrice.setColumns(10);
    txtStoresPrice.setEnabled(false);
    
    lblMotorsIcon = new JLabel("");
    lblMotorsIcon.setBounds(274, 100, 16, 16);
    getContentPane().add(lblMotorsIcon);
    
    lblShippingIcon = new JLabel("");
    lblShippingIcon.setBounds(274, 130, 16, 16);
    getContentPane().add(lblShippingIcon);
    
    lblSteelsIcon = new JLabel("");
    lblSteelsIcon.setBounds(274, 160, 16, 16);
    getContentPane().add(lblSteelsIcon);
    
    lblStoresIcon = new JLabel("");
    lblStoresIcon.setBounds(274, 190, 16, 16);
    getContentPane().add(lblStoresIcon);

    btnExit = new JButton();
    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        dispose();
      }
    });
    btnExit.setBounds(20, 385, 100, 35);
    getContentPane().add(btnExit);
    btnExit.setText("Done");
    
    lblCard = new JLabel();
    lblCard.setBounds(358, 11, 256, 400);
    getContentPane().add(lblCard);
    lblCard.setIcon(new ImageIcon(player.getCard().getCard()));
    
    //ComboBox Achieved using help from
    // - http://docs.oracle.com/javase/tutorial/uiswing/components/combobox.html
    
    String[] commodities = {"Motors", "Shipping", "Steels", "Stores"};
    
    //commodity is initialised to "motors" because combo box set by default to "Motors"
    //Otherwise a player can choose to buy/sell motors and therfore not have to select it
    //This would lead to a null pointer exception because there hasn't been an action yet
    //to actually set commodity to "motors"    
    commodity = "motors";
    
    cbxCommodities = new JComboBox(commodities);
    cbxCommodities.setBounds(192, 275, 100, 20);
    getContentPane().add(cbxCommodities);
    cbxCommodities.setSelectedIndex(0);
    
    lblQuantity = new JLabel("Quantity");
    lblQuantity.setBounds(130, 320, 100, 20);
    getContentPane().add(lblQuantity);
    cbxCommodities.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        commodity =  ( (String) cbxCommodities.getSelectedItem() ).toLowerCase();

      }
    });
    
    //set text in all text fields with dynamic content
    //used when window is rendered to set values
    //also to update values any time a players buys or sells
    setValues();
    setIcons();
  }

  /**
   * Sets the player field
   * 
   * @param player to be entered
   */
  public void setPlayer(Player player) {
    this.player = player;
  }

  /**
   * Returns the current player
   * 
   * @return the player currently taking their turn
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Sets the stockMarket field
   * 
   * @param stockMarket the stockMarket to be entered
   */
  public void setStockMarket(StockMarket stockMarket) {
    this.stockMarket = stockMarket;
  }

  /**
   * Returns the stock market
   * 
   * @return the stock market passed player is trading on
   */
  public StockMarket getStockMarket() {
    return stockMarket;
  }

  /**
   * Setter for the current commodity
   * Called by action listner to set commodities to the
   * commmodity selected in combo box
   * 
   * @param commodity The commodity to enter
   */
  public void setCommodity(String commodity) {
    this.commodity = commodity;
  }

  /**
   * Gets the currently selected commodity
   * i.e. the commodity currently selected in the combo box
   * 
   * @return The current commodity
   */
  public String getCommodity() {
    return commodity;
  }
  
  /**
   * Setter for an array of old share prices
   * 
   * @param oldPrices The array to be entered as the previous share prices
   */
  public void setOldPrices(int[] oldPrices) {
    this.oldPrices = oldPrices;
  }
  
  /**
   * Sets the text that appears in all fields with dynamic content
   * Can be called to update fields when content has changed
   */
  public void setValues() {
    txtMotorsShares.setText("" + player.getPlayerMotors());
    txtShippingShares.setText("" + player.getPlayerShipping());
    txtSteelsShares.setText("" + player.getPlayerSteel());
    txtStoresShares.setText("" + player.getPlayerStore());
    
    txtMotorsPrice.setText("$" + stockMarket.getStockPrice("motors"));
    txtShippingPrice.setText("$" + stockMarket.getStockPrice("shipping"));
    txtSteelsPrice.setText("$" + stockMarket.getStockPrice("steels"));
    txtStoresPrice.setText("$" + stockMarket.getStockPrice("stores"));
    
    txtBalance.setText("$" + player.getPlayerMoney());    
  }
  
  /**
   * Check the current share prices against the array of old prices
   * Displays icons accordingly indicating any rise or fall in share prices
   */
  public void setIcons() {
    
    Image up = new ImageIcon(this.getClass().getResource("/up.png")).getImage();
    Image down = new ImageIcon(this.getClass().getResource("/down.png")).getImage();
    
    if (oldPrices[0] < stockMarket.getStockPrice("motors")) {
      lblMotorsIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[0] > stockMarket.getStockPrice("motors")) {
      lblMotorsIcon.setIcon(new ImageIcon(down));
    } else { //if old price and current price are equal, don't show any arrow
      lblMotorsIcon.setIcon(null);
    }
    
    if (oldPrices[1] < stockMarket.getStockPrice("shipping")) {
      lblShippingIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[1] > stockMarket.getStockPrice("shipping")) {
      lblShippingIcon.setIcon(new ImageIcon(down));
    } else {
      lblShippingIcon.setIcon(null);
    }
    
    if (oldPrices[2] < stockMarket.getStockPrice("steels")) {
      lblSteelsIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[2] > stockMarket.getStockPrice("steels")) {
      lblSteelsIcon.setIcon(new ImageIcon(down));
    } else {
      lblSteelsIcon.setIcon(null);
    }
    
    if (oldPrices[3] < stockMarket.getStockPrice("stores")) {
      lblStoresIcon.setIcon(new ImageIcon(up));
    } else if (oldPrices[3] > stockMarket.getStockPrice("stores")) {
      lblStoresIcon.setIcon(new ImageIcon(down));
    } else {
      lblStoresIcon.setIcon(null);
    }
  }
  
  /**
   * Method for player to buy stock
   */
  public void buy() {

    //FIRST ERROR CHECK
    //check int entered for quantity    
    // Parsing string as integer achieved using help from
    // - http://stackoverflow.com/questions/5585779/converting-string-to-int-in-java
    int quantity = 0;
    boolean validQuantity = false;
    try {
      quantity = Integer.parseInt(txtQuantity.getText());
      validQuantity = true;

    } catch (Exception e) {
      // Achieved using help from
      // - http://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html
      JOptionPane.showMessageDialog(null, "Error: Quantity must be a number.");
    }
    
    //SECOND ERROR CHECK
    //check if player has enough money for purchase
    boolean enoughMoney = false;
    if ( player.getPlayerMoney() >=
        ( quantity * stockMarket.getStockPrice(commodity) ) ) {
      enoughMoney = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: You dont't have enough money to buy this stock.");
    }
    
    //THIRD ERROR CHECK
    //check if there is enough stock available to buy
    boolean enoughStock = false;
    if ( stockMarket.getStockShares(commodity) >= quantity ) {
      enoughStock = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: There is not enough available stock to buy"
          + "\nPlease try a smaller amount.");
    }
    
    //FOURTH ERROR CHECK
    //check if player is attempting to buy a quantity allowed by the rules
    //i.e. in each turn can buy less than 5 or multiple of 5 stock in each commodity
    boolean quantityAllowed = false;   
    int totalShares = getStockBought(commodity) + quantity;
    
    if ( ( 0 < totalShares ) && ( totalShares <= 5 ) 
        || ( quantity % 5 == 0 ) ) {
      quantityAllowed = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: You cannot buy this amount of stock"
          + "\nYou can buy stock in multiples of 5 or any amount less than 5.");
    }
    
    //FIFTH ERROR CHECK
    //check if player is trying to buy stock that is currently worthless
    boolean commodityHasValue = false;
    if (stockMarket.getStockPrice(commodity) > 0) {
      commodityHasValue = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: You cannot buy stock that has no value");
    }

    //Buy Stock
    if ( validQuantity && enoughMoney && enoughStock && quantityAllowed ) {
      // buy stock
      stockMarket.playerBuyStock(quantity, commodity, player);
      updateStocksBought(commodity, quantity);

      // add transaction to players list of transaction
      getPlayer().addPlayerTransaction(
          getPlayer().getPlayerName() + " buys " + quantity
              + " of " + commodity + " stock\n");
    }
  }
  
  /**
   * Method for player to sell stock
   */
  public void sell() {
    
    //FIRST ERROR CHECK
    //check int entered for quantity
    int quantity = 0;
    boolean validQuantity = false;
    try {
      quantity = Integer.parseInt(txtQuantity.getText());
      validQuantity = true;

    } catch (Exception e) {
      // Achieved using help from
      // - http://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html
      JOptionPane.showMessageDialog(null, "Error: Quantity must be a value");
    }
    
    //SECOND ERROR CHECK
    //check if player has the amount of stock they're trying to sell
    boolean enoughStock = false;
    if ( quantity <= player.getAnyShares(commodity) ) {
      enoughStock = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: You do not have this much stock to sell");
    }
    
    //THIRD ERROR CHECK
    //check if stock price is not zero
    boolean stockNotWorthless = false;
    if (stockMarket.getStockPrice(commodity) > 0) {
      stockNotWorthless = true;
    } else {
      JOptionPane.showMessageDialog(null, "Error: You cannot sell this stock.\n"
          + "It currently has no value.");
    }
    

    if (validQuantity && enoughStock && stockNotWorthless) {
      // sell stock
      stockMarket.playerSellStock(quantity, commodity , player);

      // add transaction to players list of transaction
      getPlayer().addPlayerTransaction(
          getPlayer().getPlayerName() + " sells " + txtQuantity.getText()
              + " of " + commodity + " stock\n");
    }
  }
  
  /**
   * Gets the total stock bought on this turn for a given commodity
   * 
   * @param commodity The commodity which stock purchases are to be returned
   * @return The number of stock already bought of the enterd commodity on this turn
   */
  public int getStockBought(String commodity)
  {
    if (commodity.equals("stores"))
    {
      return storesBought;
    }
    else if (commodity.equals("motors"))
    {
      return motorsBought;
    }
    else if (commodity.equals("steels"))
    {
      return steelsBought;
    }
    else{
      return shippingBought;
    }
  }
  
  /** Keeps track of shares bought of each commodity in the current turn
   * Facilitates the rule of only buying shares in the range [1,2,3,4,5,10,15,20,25]
   * in a given turn
   * 
   * @param commodity The commodity of which stock has been purchased
   * @param quantity The number of shares purchased
   */
  public void updateStocksBought(String commodity, int quantity) {
    
    if (commodity.equals("motors")) {
      motorsBought += quantity;
    } else if (commodity.equals("shipping")) {
      shippingBought += quantity;
    } else if (commodity.equals("stores")) {
      storesBought += quantity;
    } else {
      steelsBought += quantity;
    }
  }
}