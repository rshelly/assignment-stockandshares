import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.util.ArrayList;

/**
 * @author  Adam Buckley, Robert Shelly, Alan Walsh, Gavin Fennelly.
 * @version    1.0.0
 */

public class EnterName extends JDialog {
  
  private ArrayList<Player> players;
  private Player player;
  private int playerIndex;
  
  private JButton btnEnter;
  private JLabel lblplayerIndex;
  private JLabel lblEnterName;
  private JTextField txtName;

  /**
   * Launches the dialog box
   * Takes in an ArrayList of players and an index number as parameters
   * Calls the constructor to build the dialog box
   * 
   * @param players The ArrayList of players playing the game
   * @param playerIndex The index number of the current player
   */
  public static void main(ArrayList<Player> players, int playerIndex) {
    try {
      EnterName dialog = new EnterName(players, playerIndex);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setModalityType(ModalityType.APPLICATION_MODAL);
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Constructor for the dialog box
   * Takes in an ArrayList of players and an index number as parameters
   *  and sets them in the class fields so they can be used in all methods
   * 
   * @param players The ArrayList of players
   * @param playerIndex Index number of the current player in the ArrayList
   */
  public EnterName(ArrayList<Player> players, int playerIndex) {
    getContentPane().setBackground(Color.RED);
    setBounds(100, 100, 320, 240);
    getContentPane().setLayout(null);
    
    setPlayers(players);
    setPlayerIndex(playerIndex);
    setPlayer();
    
    btnEnter = new JButton("Enter");
    btnEnter.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        
        //First check if player entered some text into text box
        // (to avoid blank names)
        boolean validName = false;
        if (txtName.getText().trim().length() > 0) {
          validName = true;
        } else {
          JOptionPane.showMessageDialog(null, "Error: Please Enter a Name!");
        }
        
        //Check to see if another player has entered the same name
        //Duplicate names cause problems for the program
        boolean distinctName = checkDistinctName();
        
        
        if (!distinctName && (getPlayerIndex() > 0 ) ) {
          JOptionPane.showMessageDialog(null, "Unfortunately another player has used that name"
              + "\nPlease choose another name.");
        }

        if  (validName && (distinctName  || (getPlayerIndex() == 0 ) ) ) {
          setName();
          dispose();
        } 
      }
    });
    btnEnter.setBounds(205, 167, 89, 23);
    getContentPane().add(btnEnter);
    
    lblplayerIndex = new JLabel("New label");
    lblplayerIndex.setFont(new Font("Tahoma", Font.BOLD, 32));
    lblplayerIndex.setBounds(30, 30, 200, 40);
    getContentPane().add(lblplayerIndex);
    lblplayerIndex.setText("Player " + (getPlayerIndex() + 1));
    
    lblEnterName = new JLabel("New label");
    lblEnterName.setFont(new Font("Tahoma", Font.PLAIN, 12));
    lblEnterName.setBounds(30, 81, 150, 15);
    getContentPane().add(lblEnterName);
    lblEnterName.setText("Please enter your name!");
    
    txtName = new JTextField();
    txtName.setFont(new Font("Tahoma", Font.PLAIN, 32));
    txtName.setBackground(new Color(255, 255, 255));
    txtName.setForeground(new Color(0, 0, 0));
    txtName.setBounds(30, 107, 200, 40);
    getContentPane().add(txtName);
    txtName.setColumns(10);
  }
  
  /**
   * Sets the list of players
   * Takes the list pass to dialog box as a parameter
   * and sets it as field so it can be used in other methods
   * 
   * @param players The list of players to enter
   */
  public void setPlayers(ArrayList<Player> players) {
    this.players = players;
  }
  
  /**
   * Gets the list of current players
   * 
   * @return ArrayList of current players
   */
  public ArrayList<Player> getPlayers() {
    return players;
  }
  
  /**
   * Getter for the player
   * 
   * @return The current player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Sets the player field
   * This is the player currently entering their name
   * 
   * Gets the player from the list of players using the player index field
   * 
   * @param player The player to enter
   */
  public void setPlayer() {
    player = players.get(playerIndex);
  }
  
  /**
   * Getter for the player's index
   * 
   * @return The current player's index
   */
  public int getPlayerIndex() {
    return playerIndex;
  }
  
  /**
   * Sets the player's index
   * 
   * @param playerIndex The index of the current player
   */
  public void setPlayerIndex(int playerIndex) {
    this.playerIndex = playerIndex;
  }
  
  /**
   * Checks if the name entered by a player has already been entered by another player
   * Duplicate names cause problems and so must be avoided
   * 
   * @return True if name entered is distinct
   */
  public boolean checkDistinctName() {
    for (int i = 0; i <= playerIndex; i++) {
      if (txtName.getText().trim().equals(players.get(i).getPlayerName() ) ){
        return false;
      }
    }
    return true;
  }
  
  /**
   * Sets the name of the current player to the text entered in txtName
   */
  public void setName() {
    player.setPlayerName(txtName.getText().trim());
  }
}