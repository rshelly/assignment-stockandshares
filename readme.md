# Stocks & Shares

Group assignment for Programming Fundamentals. Digital version of the board
game Stocks and Shares.

## Language

* Java

## GUI Toolkit

* Java Swing

## Authors
* Adam Buckley
* Gavin Fennelly
* Rob Shelly
* Alan Walsh